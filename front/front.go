package main

import (
	"github.com/nobonobo/spago"

	"gitlab.com/tsuchinaga/go-desktop-todo/front/components"
)

func main() {
	spago.RenderBody(&components.Hello{Name: "World"})
	select {}
}
