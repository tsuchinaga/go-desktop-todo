module gitlab.com/tsuchinaga/go-desktop-todo

go 1.15

require (
	github.com/nobonobo/spago v1.0.0
	github.com/zserge/lorca v0.1.9
	golang.org/x/net v0.0.0-20200904194848-62affa334b73 // indirect
)
