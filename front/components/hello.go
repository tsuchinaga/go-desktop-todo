//go:generate spago generate -c Hello -p components hello.html
//go:generate goimports -w hello_gen.go
package components

import (
	"encoding/json"
	"fmt"
	"syscall/js"
	"time"

	"github.com/nobonobo/spago"

	"gitlab.com/tsuchinaga/go-desktop-todo/models"
)

// Hello ...
type Hello struct {
	spago.Core
	Name string
}

func (c *Hello) Hello() string {
	return "Hello, " + c.Name
}

func (c *Hello) Add(_ js.Value) {
	b, err := json.Marshal(models.Task{Title: "Hello, Task!!", CreateTime: time.Now()})
	if err != nil {
		fmt.Println(err)
		return
	}
	js.Global().Call("add", string(b))
}
