# go desktop todo

Go言語を使ってデスクトップで動作するTODOアプリを作る

## 環境

* go version go1.15 windows/amd64
* webview
    * [zserge/lorca](https://github.com/zserge/lorca)
* spa
    * [~~nobonobo/wecty~~](https://github.com/nobonobo/wecty) spagoがv1.0.0になったので置き換え
    * [nobonobo/spago](https://github.com/nobonobo/spago)
* store
    * 未定

## 実行

* wasmの出力
    * `$ GOOS=js GOARCH=wasm go build -o ./back/www/main.wasm ./front/.`
* wasm_exex.jsをとってきてback/wwwに設置
* assetsの生成
    * `$ go generate ./back/gen/`
* 実行
    * `$ go run ./back/`