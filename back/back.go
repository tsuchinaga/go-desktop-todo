package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net"
	"net/http"

	"gitlab.com/tsuchinaga/go-desktop-todo/models"

	"github.com/zserge/lorca"
	"gitlab.com/tsuchinaga/go-desktop-todo/back/assets"
)

func main() {
	ui, err := lorca.New("", "", 400, 300)
	if err != nil {
		log.Fatalln(err)
	}
	defer ui.Close()

	srv, err := newSrv()
	if err != nil {
		log.Fatalln(err)
	}
	defer srv.close()
	go func() {
		if err := srv.run(); err != nil && err != http.ErrServerClosed {
			log.Fatal(err)
		}
	}()

	_ = ui.Bind("add", func(task string) {
		var t models.Task
		if err := json.Unmarshal([]byte(task), &t); err != nil {
			fmt.Println(err)
			return
		}
		fmt.Printf("%+v\n", t)
		ui.Eval("bye()")
	})
	if err := ui.Load(fmt.Sprintf("http://%s", srv.addr())); err != nil {
		log.Fatalln(err)
	}

	<-ui.Done()
}

func newSrv() (*srv, error) {
	ln, err := net.Listen("tcp", "127.0.0.1:0")
	if err != nil {
		return nil, err
	}

	return &srv{ln: ln, srv: &http.Server{Handler: http.FileServer(assets.FS)}}, nil
}

type srv struct {
	ln  net.Listener
	srv *http.Server
}

func (s *srv) addr() net.Addr {
	return s.ln.Addr()
}

func (s *srv) run() error {
	return s.srv.Serve(s.ln)
}

func (s *srv) close() []error {
	var ers []error
	if err := s.srv.Shutdown(context.Background()); err != nil {
		ers = append(ers, err)
	}

	if err := s.ln.Close(); err != nil {
		ers = append(ers, err)
	}

	if err := s.srv.Close(); err != nil {
		ers = append(ers, err)
	}

	return ers
}
