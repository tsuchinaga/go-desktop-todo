package models

import "time"

type Task struct {
	Title      string
	CreateTime time.Time
}
