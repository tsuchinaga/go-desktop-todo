package components

import (
	"github.com/nobonobo/spago"
)

// Render ...
func (c *Hello) Render() spago.HTML {
	return spago.Tag("div",
		spago.Tag("button",
			spago.Event("click", c.Add),
			spago.T(`Add`),
		),
	)
}
